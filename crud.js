let http = require("http")
const PORT = 3000;


let directory = [
	{"name":"Brandon", "email" :"brandon@mail.com"},
	{"name":"Robert", "email" :"robert@mail.com"}
];


http.createServer((request, response) => {
	// Retrieve our documents, url = /users, method = GET, response = array
	if(request.url === "/users" && request.method === "GET") {
		response.writeHead(200, {"Content-Type":"application/json"});
		console.log(directory)
		response.write(JSON.stringify(directory));
		response.end();
	}

	// Create a new user
		// url = /users
		// method = POST
		// data from the quest will be comeing from the client's body
			// request.body
	if(request.url === "/users" && request.method === "POST") {
		let reqBody = "";
		request.on("data", (data) => {
			reqBody += data;
		});

		request.on("end",() => {
			reqBody = JSON.parse(reqBody);
			directory.push(reqBody);
			response.writeHead(200, {"Content-Type":"application/json"})
			response.write(JSON.stringify(directory));
			response.end();
		});
	}	
}).listen(PORT);
console.log(`Server is now connected to port ${PORT}.`)