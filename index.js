/* HTTP METHODS
	- Get - retrieves resources
	- Post - sends data for creating a resource
	- Put - sends data for updating a resource
	- Delete - delete a specified resources

*/

let http = require("http");
const PORT = 3000;

http.createServer((request, response)=>{
	// uri/endpoint = resource
	// http method
	// body

	if (request.url === "/profile" && request.method === "GET") {
		response.writeHead(200, {"Content-Type":"text/html"});
		response.end("Welcome to my page");
	} else if (request.url === "/register" && request.method === "POST") {
		response.writeHead(200, {"Content-Type":"text/html"});
		response.write("Data to be sent to the database.");
		response.end();
	} else {
		response.writeHead(404, {"Content-Type":"text/html"});
		console.log(request)
		response.end("Request cannot be completed.");
	}
}).listen(PORT);
console.log(`Server is now connected to port ${PORT}`);